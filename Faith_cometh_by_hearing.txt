<item>
<title>[COLORorangered][B]-----Faith Cometh By Hearing-----[/B][/COLOR]</title>
<link> </link>
<thumbnail>https://s22.postimg.cc/76n9g2sj5/coollogo_com-5422251.png</thumbnail>
<fanart>https://i.ytimg.com/vi/m33mSAOYZIw/maxresdefault.jpg</fanart>
</item>

<item>
<title>[COLOR white]And Such Were Some of You[/COLOR]</title>
<utube>PsXhZbK0JQY</utube>
<thumbnail>https://s22.postimg.cc/76n9g2sj5/coollogo_com-5422251.png</thumbnail>
<fanart>https://i.ytimg.com/vi/m33mSAOYZIw/maxresdefault.jpg</fanart>
</item>

<item>
<title>[COLOR white]The Simplicity of Christ[/COLOR]</title>
<utube>BK5wgH99cEE</utube>
<thumbnail>https://s22.postimg.cc/76n9g2sj5/coollogo_com-5422251.png</thumbnail>
<fanart>https://i.ytimg.com/vi/m33mSAOYZIw/maxresdefault.jpg</fanart>
</item>

<item>
<title>[COLOR white]Repentance is Lifelong[/COLOR]</title>
<utube>sKjXf8YHLoo</utube>
<thumbnail>https://s22.postimg.cc/76n9g2sj5/coollogo_com-5422251.png</thumbnail>
<fanart>https://i.ytimg.com/vi/m33mSAOYZIw/maxresdefault.jpg</fanart>
</item>

<item>
<title>[COLOR white]Blood and water[/COLOR]</title>
<utube>_PolhfXvkKk</utube>
<thumbnail>https://s22.postimg.cc/76n9g2sj5/coollogo_com-5422251.png</thumbnail>
<fanart>https://i.ytimg.com/vi/m33mSAOYZIw/maxresdefault.jpg</fanart>
</item>

<item>
<title>[COLOR white]Jesus was not soft spoken[/COLOR]</title>
<utube>anoJ7Wfb5mY</utube>
<thumbnail>https://s22.postimg.cc/76n9g2sj5/coollogo_com-5422251.png</thumbnail>
<fanart>https://i.ytimg.com/vi/m33mSAOYZIw/maxresdefault.jpg</fanart>
</item>

<item>
<title>[COLOR white]The Sins That Killed Jesus[/COLOR]</title>
<utube>DHDpT7eHcJU</utube>
<thumbnail>https://s22.postimg.cc/76n9g2sj5/coollogo_com-5422251.png</thumbnail>
<fanart>https://i.ytimg.com/vi/m33mSAOYZIw/maxresdefault.jpg</fanart>
</item>

<item>
<title>[COLOR white]Is Water Baptism Necessary for salvation[/COLOR]</title>
<utube>WpoieqHdpNM</utube>
<thumbnail>https://s22.postimg.cc/76n9g2sj5/coollogo_com-5422251.png</thumbnail>
<fanart>https://i.ytimg.com/vi/m33mSAOYZIw/maxresdefault.jpg</fanart>
</item>

<item>
<title>[COLOR white]We must judge righteously[/COLOR]</title>
<utube>QwZbrztHiMk</utube>
<thumbnail>https://s22.postimg.cc/76n9g2sj5/coollogo_com-5422251.png</thumbnail>
<fanart>https://i.ytimg.com/vi/m33mSAOYZIw/maxresdefault.jpg</fanart>
</item>

<item>
<title>[COLOR white]Jesus Debated[/COLOR]</title>
<utube>OeMM2ev9unk</utube>
<thumbnail>https://s22.postimg.cc/76n9g2sj5/coollogo_com-5422251.png</thumbnail>
<fanart>https://i.ytimg.com/vi/m33mSAOYZIw/maxresdefault.jpg</fanart>
</item>

<item>
<title>[COLOR white]Grace and Law[/COLOR]</title>
<utube>8wixzL_3Chs</utube>
<thumbnail>https://s22.postimg.cc/76n9g2sj5/coollogo_com-5422251.png</thumbnail>
<fanart>https://i.ytimg.com/vi/m33mSAOYZIw/maxresdefault.jpg</fanart>
</item>

<item>
<title>[COLOR white]Did Jesus Go To Hell[/COLOR]</title>
<utube>HyIOwdFaSpk</utube>
<thumbnail>https://s22.postimg.cc/76n9g2sj5/coollogo_com-5422251.png</thumbnail>
<fanart>https://i.ytimg.com/vi/m33mSAOYZIw/maxresdefault.jpg</fanart>
</item>

<item>
<title>[COLOR white]Children are born sinless[/COLOR]</title>
<utube>kjarFB_OJAk</utube>
<thumbnail>https://s22.postimg.cc/76n9g2sj5/coollogo_com-5422251.png</thumbnail>
<fanart>https://i.ytimg.com/vi/m33mSAOYZIw/maxresdefault.jpg</fanart>
</item>

<item>
<title>[COLOR white]Church Discipline is much needed[/COLOR]</title>
<utube>kvJDuRYa_HI</utube>
<thumbnail>https://s22.postimg.cc/76n9g2sj5/coollogo_com-5422251.png</thumbnail>
<fanart>https://i.ytimg.com/vi/m33mSAOYZIw/maxresdefault.jpg</fanart>
</item>

<item>
<title>[COLOR white]Speaking The Truth in Love[/COLOR]</title>
<utube>8ioII1lmR8E</utube>
<thumbnail>https://s22.postimg.cc/76n9g2sj5/coollogo_com-5422251.png</thumbnail>
<fanart>https://i.ytimg.com/vi/m33mSAOYZIw/maxresdefault.jpg</fanart>
</item>

<item>
<title>[COLOR white]Yes A Christian can lose their salvation[/COLOR]</title>
<utube>MMoUDCGlpa8</utube>
<thumbnail>https://s22.postimg.cc/76n9g2sj5/coollogo_com-5422251.png</thumbnail>
<fanart>https://i.ytimg.com/vi/m33mSAOYZIw/maxresdefault.jpg</fanart>
</item>

<item>
<title>[COLOR white]God Does Not Speak Outside His Word[/COLOR]</title>
<utube>dMHJddAj8HM</utube>
<thumbnail>https://s22.postimg.cc/76n9g2sj5/coollogo_com-5422251.png</thumbnail>
<fanart>https://i.ytimg.com/vi/m33mSAOYZIw/maxresdefault.jpg</fanart>
</item>

<item>
<title>[COLOR white]Stop Burning Gods Word[/COLOR]</title>
<utube>YCTL_wEpkcY</utube>
<thumbnail>https://s22.postimg.cc/76n9g2sj5/coollogo_com-5422251.png</thumbnail>
<fanart>https://i.ytimg.com/vi/m33mSAOYZIw/maxresdefault.jpg</fanart>
</item>

<item>
<title>[COLOR white]Does New Testament Evangelism Work[/COLOR]</title>
<utube>yGgSfEbZjR0</utube>
<thumbnail>https://s22.postimg.cc/76n9g2sj5/coollogo_com-5422251.png</thumbnail>
<fanart>https://i.ytimg.com/vi/m33mSAOYZIw/maxresdefault.jpg</fanart>
</item>

<item>
<title>[COLOR white]Conversation With A Baptist Pastor[/COLOR]</title>
<utube>UNCN_p3nwjU</utube>
<thumbnail>https://s22.postimg.cc/76n9g2sj5/coollogo_com-5422251.png</thumbnail>
<fanart>https://i.ytimg.com/vi/m33mSAOYZIw/maxresdefault.jpg</fanart>
</item>


<item>
<title>[COLOR white]Jeffrey Dahmer Obeyed The Gospel[/COLOR]</title>
<utube>GXynllkAVOA</utube>
<thumbnail>https://s22.postimg.cc/76n9g2sj5/coollogo_com-5422251.png</thumbnail>
<fanart>https://i.ytimg.com/vi/m33mSAOYZIw/maxresdefault.jpg</fanart>
</item>

<item>
<title>[COLOR white]1877 Baptist Scholar admits Baptism Is For Salvation[/COLOR]</title>
<utube>sdoG_hlEplU</utube>
<thumbnail>https://s22.postimg.cc/76n9g2sj5/coollogo_com-5422251.png</thumbnail>
<fanart>https://i.ytimg.com/vi/m33mSAOYZIw/maxresdefault.jpg</fanart>
</item>

<item>
<title>[COLOR white]Romans 10:13 Is not a prayer[/COLOR]</title>
<utube>mpoCODeEmsI</utube>
<thumbnail>https://s22.postimg.cc/76n9g2sj5/coollogo_com-5422251.png</thumbnail>
<fanart>https://i.ytimg.com/vi/m33mSAOYZIw/maxresdefault.jpg</fanart>
</item>

<item>
<title>[COLOR white]What is calling On The Name of the Lord[/COLOR]</title>
<utube>8GKOUBaf42Y</utube>
<thumbnail>https://s22.postimg.cc/76n9g2sj5/coollogo_com-5422251.png</thumbnail>
<fanart>https://i.ytimg.com/vi/m33mSAOYZIw/maxresdefault.jpg</fanart>
</item>

<item>
<title>[COLOR white]The Gift in Acts Is Miraculous Ability[/COLOR]</title>
<utube>h116itL2ITo</utube>
<thumbnail>https://s22.postimg.cc/76n9g2sj5/coollogo_com-5422251.png</thumbnail>
<fanart>https://i.ytimg.com/vi/m33mSAOYZIw/maxresdefault.jpg</fanart>
</item>

<item>
<title>[COLOR white]The church is not a building[/COLOR]</title>
<utube>fsBfNWV9aGg</utube>
<thumbnail>https://s22.postimg.cc/76n9g2sj5/coollogo_com-5422251.png</thumbnail>
<fanart>https://i.ytimg.com/vi/m33mSAOYZIw/maxresdefault.jpg</fanart>
</item>

<item>
<title>[COLOR white]God said assemble together[/COLOR]</title>
<utube>d8hEdCcUWcw</utube>
<thumbnail>https://s22.postimg.cc/76n9g2sj5/coollogo_com-5422251.png</thumbnail>
<fanart>https://i.ytimg.com/vi/m33mSAOYZIw/maxresdefault.jpg</fanart>
</item>

<item>
<title>[COLOR white]You are not baptized with the Holy Spirit[/COLOR]</title>
<utube>-sWxIgT8OHg</utube>
<thumbnail>https://s22.postimg.cc/76n9g2sj5/coollogo_com-5422251.png</thumbnail>
<fanart>https://i.ytimg.com/vi/m33mSAOYZIw/maxresdefault.jpg</fanart>
</item>

<item>
<title>[COLOR white]Covid-19? Christ will return! No signs[/COLOR]</title>
<utube>CMX7wu9CsU0</utube>
<thumbnail>https://s22.postimg.cc/76n9g2sj5/coollogo_com-5422251.png</thumbnail>
<fanart>https://i.ytimg.com/vi/m33mSAOYZIw/maxresdefault.jpg</fanart>
</item>

<item>
<title>[COLOR white]You must be baptized to be saved[/COLOR]</title>
<utube>pIECu9Op2CI</utube>
<thumbnail>https://s22.postimg.cc/76n9g2sj5/coollogo_com-5422251.png</thumbnail>
<fanart>https://i.ytimg.com/vi/m33mSAOYZIw/maxresdefault.jpg</fanart>
</item>

<item>
<title>[COLOR white]The Kingdom is the church of Christ[/COLOR]</title>
<utube>Iwe1FR75ZUs</utube>
<thumbnail>https://s22.postimg.cc/76n9g2sj5/coollogo_com-5422251.png</thumbnail>
<fanart>https://i.ytimg.com/vi/m33mSAOYZIw/maxresdefault.jpg</fanart>
</item>

<item>
<title>[COLOR white]What is the church of Christ trying to Accomplish[/COLOR]</title>
<utube>EGC72kHX-uQ</utube>
<thumbnail>https://s22.postimg.cc/76n9g2sj5/coollogo_com-5422251.png</thumbnail>
<fanart>https://i.ytimg.com/vi/m33mSAOYZIw/maxresdefault.jpg</fanart>
</item>

